#!/usr/bin/env bash

set -e
set -x

: "${INFLUX_BUCKET:?Variable not set or empty}"
: "${INFLUX_ORG:?Variable not set or empty}"
: "${INFLUX_USER:?Variable not set or empty}"
: "${INFLUX_PASS:?Variable not set or empty}"
: "${INFLUX_TOKEN:?Variable not set or empty}"
: ${INFLUX_HOST:="http://influx:8086"}

influx ping --host ${INFLUX_HOST}

influx setup \
-f \
-t ${INFLUX_TOKEN} \
--host ${INFLUX_HOST} \
-b ${INFLUX_BUCKET} \
-o ${INFLUX_ORG} \
-u ${INFLUX_USER} \
-p ${INFLUX_PASS} || true

# Add a Database / Retention Policy mapping for v1 compatibility

BUCKET_ID=$(influx bucket list --name ${INFLUX_BUCKET} --hide-headers | awk '{print $1}' || "")

if [ -n "$BUCKET_ID" ]; then
    influx v1 dbrp create \
    --db ${INFLUX_BUCKET} \
    --rp rp \
    --bucket-id ${BUCKET_ID} \
    --default || true
fi
